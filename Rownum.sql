SELECT ROWNUM AS "Longest employed", last_name, hire_date
FROM (SELECT last_name, hire_date
FROM employees
ORDER BY hire_date)
WHERE ROWNUM <=5;