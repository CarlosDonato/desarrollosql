CREATE OR REPLACE VIEW view_dept50
AS SELECT department_id, employee_id,first_name, last_name, salary
FROM copy_employees
WHERE department_id= 50
WITH READ ONLY;

SELECT * FROM view_dept50;

UPDATE view_dept50
SET department_id= 90
WHERE employee_id= 124;