SELECT last_name || TO_CHAR(salary,'$99,999') AS "Apellido y Salario", hire_date "Contratación", hire_date + 60 "Fecha de aumento" , TO_CHAR(salary +2000,'$99,999') "Salario aumentado"
FROM employees
WHERE hire_date >= '01/01/08';