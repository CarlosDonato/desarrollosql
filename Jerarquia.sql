SELECT LPAD(last_name, LENGTH(last_name)+
(LEVEL*2)-2,' ') AS "Org_Chart"
FROM employees
START WITH last_name = 'King'
CONNECT BY PRIOR employee_id = manager_id;